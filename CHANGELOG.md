# Changelog

## v1.7.8

* fix whelled vehicles
* fix localization

## v1.7.7

* fix swap hangar feature
* fix vehicle present bug

## v1.7.6

* added swap hangar feature
* fix YOH vehicles bug
* fix vehicle rebuild bug
* fix vehicle insignia rank display bug
* fix out richoshet bug
* added ukranian localization
* changed ui sound theme

## v1.7.5

* fix out richoshet display logic
* fix shot result labels
* fix language initialization

## v1.7.4

* fix vehicle load bug
* space binary

## v1.7.3

* added simplified Chinese translation (ty for @cryse)
* space binary

## v1.7.2

* fix vehicle assamble on first load

## v1.7.1

* fix camera forced update
* fix component matrix bug
* validate preview vehicle with simplified compactDescr
* vehicle present validator (help wooden PC)

## v1.7.0

* fix crash (undisposed elements cause crash randomly)
* fix hit validation

## v1.6.9

* fix camera force update intervals
* use ingame vehicle assambler

## v1.6.8

* fix steelhunter

## v1.6.7

* fix forced camera position
* fix errors in log on mod UI close
* fix crashes on GameObject no longer available
* fix UI fonts

## v1.6.6

* repo moved to gitlab