package com.poliroid.gui.lobby.battleHits.data
{
	import net.wg.data.daapi.base.DAAPIDataClass;

	public class BatHitsDetailedHitVO extends DAAPIDataClass
	{
		public var noDataLabel:String = "";

		public function BatHitsDetailedHitVO(data:Object): void
		{
			super(data);
		}
	}
}
